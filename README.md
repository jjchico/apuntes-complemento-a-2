# Representación binaria de números enteros en complemento a 2

Este documento contiene un conjunto de propiedades útiles para la representación binaria de números enteros en complemento a 2. El documento se centra en las propiedades y sus demostraciones con breves notas sobre sus aplicaciones y/o utilidad.

La intención de este documento es servir como material complementario en cursos que incluyan la representación en complemento a 2 en sus contenidos.

## Generar documento

Puede generar un documento a partir del código fuente mediante el sistema de procesamiento de texto LaTeX. Por ejemplo, en sistemas con el sistema *latexmk* puede ejecutar:

    $ latexmk -pdf apuntes-complemento-a-2.tex

Alternativamente puede descargar un PDF generado automáticamente desde https://gitlab.com/jjchico/apuntes-complemento-a-2/-/jobs/artifacts/master/browse?job=build_pdf


## Contribuciones

Puede clonar este repositorio y enviar solicitudes de integración que serán evaluadas por el autor.

## Autores y créditos

* Autor: Jorge Juan-Chico <jjchico@dte.us.es>

## Licencia

Este obra está bajo una licencia de Creative Commons Reconocimiento-CompartirIgual 4.0 Internacional. Usted es libre de compartir y modificar esta obra siempre que cite al autor original y comparta la obra derivada con la misma licencia.

Véase http://creativecommons.org/licenses/by-sa/4.0/ para más información.
